;;; .emacs.el --- my init file

;;; Commentary:
;;; Code:
(setq inhibit-startup-message t)
(prefer-coding-system 'utf-8-unix)
(setq eval-expression-print-length nil)
(setq eval-expression-print-level nil)
(setq-default tab-width 4 indent-tabs-mode nil)
(add-hook 'before-save-hook 'delete-trailing-whitespace)
(keyboard-translate ?\C-h ?\C-?)
(global-set-key "\C-m" 'newline-and-indent)
(global-set-key [f1] 'help-for-help)
(show-paren-mode 1)
(tool-bar-mode -1)
(column-number-mode 1)
(defvar browse-url-firefox-program "firefox-beta")
(defvar browse-url-default-browser 'browse-url-firefox-program)

(let ((font "DejaVu Sans Mono")
      (size "17"))
  (when (member font (font-family-list))
    (set-face-attribute 'default nil :font (concat font "-" size))))
(when window-system (set-frame-size (selected-frame) 70 35))


(add-to-list 'auto-mode-alist '("\\.txt$" . org-mode))
(global-set-key "\C-ca" 'org-agenda)
(global-set-key "\C-cl" 'org-store-link)
(global-set-key "\C-cb" 'org-iswitchb)
(add-hook 'compilation-mode-hook
          (lambda () (visual-line-mode 1)))
(add-hook 'compilation-minor-mode-hook
          (lambda () (visual-line-mode 1)))
(defalias 'perl-mode 'cperl-mode)
(require 'generic-x)

;;; emacsclientとか
(progn
  (require 'server)
  (unless (server-running-p)
    (server-start))
  (global-set-key (kbd "C-x C-c") 'server-edit)
  (defalias 'exit 'save-buffers-kill-emacs))

(with-eval-after-load "diff-mode"
  (add-to-list 'load-path "~/.emacs.d/site-lisp/diff")
  (load "my-diff"))
(with-eval-after-load "org"
  (add-to-list 'load-path "~/.emacs.d/site-lisp/org")
  (load "org-fmt-diff")
  (load "org-replaced-ff-odt-export-to-odt"))
(with-eval-after-load "mercurial"
  (add-to-list 'load-path "~/.emacs.d/site-lisp/mercurial")
  (load "hg-grep"))

(advice-add
 'setenv :around
 (lambda (f &rest args)
   "discard GREP_OPTIONS on grep"
   (let ((VARIABLE (car args)))
     (unless (string= VARIABLE "GREP_OPTIONS")
       (apply f args)))))
(advice-add
 'find-tag :before
 (lambda (&rest args)
   (let ((default-directory (file-name-directory buffer-file-name)))
     (unless (file-exists-p "TAGS")
       (shell-command "ctags -eR"))
     (visit-tags-table "TAGS"))))


(require 'package)
(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/"))
(package-initialize)

(define-key global-map (kbd "C-c C-r") 'redpen-paragraph)
(with-eval-after-load "org"
  (defvar org-mode-map)
  ;; Override `org-reveal' by `global-map' or set other key.
  (define-key org-mode-map (kbd "C-c C-r") nil))

(require 'auto-complete)
(require 'auto-complete-config) (ac-config-default)
(ac-flyspell-workaround)
(push 'auto-complete completion-at-point-functions)
(add-to-list 'ac-dictionary-directories "~/.emacs.d/ac-dict")
(let ((dir (car ac-dictionary-directories)))
  (unless (file-exists-p dir)
    (make-directory dir)))
;; for auto-complete source from yasnippet dir
(defvar yas-snippet-dirs '("~/.emacs.d/snippets"))
(let ((dir (car yas-snippet-dirs)))
  (unless (file-exists-p dir)
    (make-directory dir)))
(require 'yasnippet) (yas-minor-mode 1)
(global-auto-complete-mode t)

(require 'recentf-ext)
(require 'elscreen) (elscreen-start)
(require 'flycheck) (global-flycheck-mode t)
(add-hook 'text-mode-hook 'flyspell-mode)
(add-hook 'prog-mode-hook 'flyspell-prog-mode)

(autoload 'open-junk-file "open-junk-file")
(defvar open-junk-file-format "~/junk/%Y-%m-%d-%H%M%S.")
(global-set-key (kbd "C-x j") 'open-junk-file)

(progn
  (global-set-key (kbd "C-x J") 'skk-auto-fill-mode)
  (setq default-input-method "japanese-skk") ;; C-\
  (defvar skk-sticky-key ";")
  (defvar skk-large-jisyo
    (expand-file-name
     "SKK-JISYO.L"
     (if (eq system-type 'windows-nt)
         "C:/msys64/usr/share/skk" "/usr/share/skk")))
  (defvar skk-dcomp-activate t)
  (defvar skk-show-annotation t)
  (defvar skk-kuten-touten-alist
        '((jp . ("。" . "、"))
          (jp2 . ("．" . "、"))
          (en . ("．" . "，"))))
  (defvar skk-kutouten-type 'en)
  (setq-default skk-kutouten-type 'en)
  (defvar skk-auto-insert-paren t))

(require 'helm-config)
(helm-mode 1)
(define-key global-map [remap find-file] 'helm-find-files)
(define-key global-map [remap list-buffers] 'helm-buffers-list)
(define-key global-map [remap dabbrev-expand] 'helm-dabbrev)
(define-key global-map [remap occur] 'helm-occur)
(global-set-key (kbd "C-M-o") 'helm-occur)
(define-key isearch-mode-map (kbd "C-o") 'helm-occur-from-isearch)
(global-set-key (kbd "C-x C-r") 'helm-recentf)
(global-set-key (kbd "M-x") 'helm-M-x)
(global-set-key (kbd "M-y") 'helm-show-kill-ring)
(global-set-key (kbd "C-c i") 'helm-imenu)

(unless (boundp 'completion-in-region-function)
  (define-key lisp-interaction-mode-map
    [remap completion-at-point] 'helm-lisp-completion-at-point)
  (define-key emacs-lisp-mode-map
    [remap completion-at-point] 'helm-lisp-completion-at-point))

(progn
  ;;括弧の対応を保持して編集する設定
  (autoload 'enable-paredit-mode "paredit"
    "Turn on pseudo-structural editing of Lisp code." t)
  (add-hook 'emacs-lisp-mode-hook 'enable-paredit-mode)
  (autoload 'clojure-mode "clojure-mode" "Major mode for clojure." t)
  (add-to-list 'auto-mode-alist '("\\.clj$" . clojure-mode))
  (add-hook 'emacs-lisp-mode-hook 'enable-paredit-mode)
  (add-hook 'lisp-interaction-mode-hook 'enable-paredit-mode)
  (add-hook 'lisp-mode-hook 'enable-paredit-mode)
  (add-hook 'ielm-mode-hook 'enable-paredit-mode)
  (add-hook 'clojure-mode-hook 'enable-paredit-mode)

  ;;式の評価結果を注釈するための設定
  (autoload 'lispxmp "lispxmp")
  ;;emacs-lisp-modeでC-c C-dを押すと注釈される
  (define-key emacs-lisp-mode-map (kbd "C-c C-d") 'lispxmp) ; =>
  (define-key lisp-interaction-mode-map (kbd "C-c C-d") 'lispxmp) ; =>
  (global-set-key "\C-cd" 'eval-print-last-sexp)

  (autoload 'enable-auto-async-byte-compile-mode
    "auto-async-byte-compile")
  (add-hook 'emacs-lisp-mode-hook
            'enable-auto-async-byte-compile-mode))

(progn
  (require 'popwin)
  (popwin-mode 1)
  (push '(dired-mode :position top) popwin:special-display-config)
  (push "*Diff*" popwin:special-display-config)
  ;; M-x dired-jump-other-window
  (push '(dired-mode :position top) popwin:special-display-config)
  ;; M-!
  (push "*Shell Command Output*" popwin:special-display-config)
  ;; M-x compile
  (push '(compilation-mode :noselect t) popwin:special-display-config))

(defvar twittering-icon-mode t)
(defvar twittering-use-master-password t)

(add-hook 'sgml-mode-hook 'emmet-mode) ;; Auto-start on any markup modes
(add-hook 'html-mode-hook 'emmet-mode)
(add-hook 'css-mode-hook  'emmet-mode)

(add-hook 'after-init-hook 'helm-recentf)

;; Local Variables:
;; coding: utf-8
;; End:

;;; .emacs.el ends here

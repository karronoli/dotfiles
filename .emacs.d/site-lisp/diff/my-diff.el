;; http://www.mag2.com/m/0001373131.html
(defvar ediff-window-setup-function 'ediff-setup-windows-plain)

(defun diff-mode-setup-faces ()
  ;; 追加された行は緑で表示
  (set-face-attribute 'diff-added nil
                      :foreground "white" :background "dark green")
  ;; 削除された行は赤で表示
  (set-face-attribute 'diff-removed nil
                      :foreground "white" :background "dark red")
  ;; 文字単位での変更箇所は色を反転して強調
  (set-face-attribute 'diff-refine-change nil
                      :foreground nil :background nil
                      :weight 'bold :inverse-video t))

;; diffを表示したらすぐに文字単位での強調表示も行う
(autoload 'diff-auto-refine-mode "diff-mode")

(add-hook 'diff-mode-hook
          (lambda()
            (diff-mode-setup-faces)
            (setq truncate-lines t)
            (diff-auto-refine-mode t)))

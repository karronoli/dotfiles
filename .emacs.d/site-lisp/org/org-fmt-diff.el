(defun org-fmt-diff ()
  "Diff between the current buffer and org-org-export-as-org result."
  (interactive)
  (require 'ox-org)
  (let ((input (buffer-name))
        (formatted "*Org to Org formatted*")
        (org-export-show-temporary-export-buffer nil)
        (truncate-lines nil))
    (org-export-to-buffer 'org formatted)
    (if (string= (with-current-buffer input
                   (buffer-string))
                 (with-current-buffer formatted
                   (buffer-string))) (kill-buffer formatted)
      (show-all)
      (org-show-block-all)
      (diff (get-buffer input) (get-buffer formatted))))
  nil)

(add-hook 'org-mode-hook
          (lambda ()
            (add-hook 'before-save-hook 'org-fmt-diff nil 'make-it-local)))

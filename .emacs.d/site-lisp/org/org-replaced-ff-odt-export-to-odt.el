(defun org-replaced-ff-odt-export-to-odt (f &rest args)
  "Generate replace FormFeed to ODT PageBreak"
  (let* ((bfn (buffer-file-name))
         (temp (make-temp-file (expand-file-name "org" ".")))
         (odt (concat temp ".odt")))
    (copy-file bfn temp t)
    (with-current-buffer (find-file-noselect temp)
      (goto-char (point-min))
      (while
          (search-forward "\f" nil t)
        (replace-match
         (concat
          "#+ODT_STYLES_FILE: \"styles.xml\"\n"
          "#+ODT: <text:p text:style-name=\"PageBreak\"/>"
          )))
      (save-buffer)
      (apply f args))
    (unless (file-exists-p odt)
      (error "Generate odt failure."))
    (copy-file odt
               (concat (file-name-base bfn) ".odt")
               t)
    )
  t)

(advice-add 'org-odt-export-to-odt
            :around 'org-replaced-ff-odt-export-to-odt)

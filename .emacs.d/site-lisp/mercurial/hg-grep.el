(require 'grep)
(autoload 'hg-root "mercurial")
(autoload 'hg-binary "mercurial")

(defvar hg-grep-history nil "History list for hg grep.")
(defvar hg-grep-nkf "nkf -w8")
(defun hg-grep (grep-dir command-args)
  "Grep the repository by hg grepfile"
  (interactive
   (list
    (or (hg-root)
        (read-file-name
         "Directory for hg grepfile: " "/" "/" t))
    (read-shell-command
     "Run hg grepfile (like this): "
     (format "%s grepfile -n -i %s"
             (hg-binary) "")
     'hg-grep-history)))
  (let ((grep-use-null-device nil)
        (command
         (format "%s -R %s | %s" command-args grep-dir hg-grep-nkf)))
    (grep command)))

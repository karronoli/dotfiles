# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=100000
SAVEHIST=100000
setopt appendhistory extendedglob notify
unsetopt autocd beep nomatch
bindkey -e
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/karo/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

ulimit -n 2048
export EDITOR=vim
export WINEARCH=win32

alias grep='grep --color=auto'
alias ls='ls --color=auto'
alias chromium='env LANG=en_US.UTF-8 chromium'
alias scilab='scilab -l en_US'
alias poweroff='systemctl poweroff'
alias fc-list-ja='fc-list :lang=ja'
alias diff-dotfiles='pushd ~/dotfiles>/dev/null && find . -name .hg -prune -o -type f -print | xargs -n1 -I{} sh -c "diff -u {} ../{}" ; hg diff ; popd>/dev/null'
alias redpen-update='pushd /tmp && git clone git@github.com:karronoli/redpen.git ; cd redpen && git remote add upstream git://github.com/redpen-cc/redpen.git && git pull upstream master && git push origin master && popd'

function md() {
  mkdir -p "$@" && cd "$1"
}

